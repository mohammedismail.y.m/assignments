package com.ey.builder;

public class Limca extends ColdDrink{
	   @Override
	   public float price() {
	      return 35.0f;
	   }
	 
	   @Override
	   public String name() {
	      return "Cold Drink Limca";
	   }
}

