package com.ey.builder;

public class BuilderPatternTest {

	public static void main(String[] args) {
	
		MealBuilder mealBuilder = new MealBuilder();
		 
	      Meal vegMeal = mealBuilder.buildVegMeal();
	      System.out.println("Veg Meal");
	      vegMeal.displayItems();
	      System.out.println("Total Cost: " + vegMeal.getCost());
	 
	      Meal nonVegMeal = mealBuilder.buildNonVegMeal();
	      System.out.println("NonVeg Meal");
	      nonVegMeal.displayItems();
	      System.out.println("Total Cost: " + nonVegMeal.getCost());
	}

}

