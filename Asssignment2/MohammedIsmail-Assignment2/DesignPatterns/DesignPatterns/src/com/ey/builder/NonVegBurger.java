package com.ey.builder;

public class NonVegBurger extends Burger {
	   @Override
	   public float price() {
	      return 100.0f;
	   }
	 
	   @Override
	   public String name() {
	      return "NonVeg Burger";
	   }
}
