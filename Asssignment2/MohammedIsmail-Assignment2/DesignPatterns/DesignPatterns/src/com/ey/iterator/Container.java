package com.ey.iterator;

public interface Container {

	public Iterator getIterator();
}
