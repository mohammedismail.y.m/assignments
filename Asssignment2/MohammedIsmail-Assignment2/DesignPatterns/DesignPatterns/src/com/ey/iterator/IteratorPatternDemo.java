package com.ey.iterator;

public class IteratorPatternDemo {

	public static void main(String[] args) {
		
		  CollectionofNames cmpny = new CollectionofNames();  
          
          for(Iterator iter = cmpny.getIterator(); iter.hasNext();){  
              String name = (String)iter.next();  
              System.out.println("Name : " + name);   
              
           }      

	} 

}

