package com.ey.singleton;

public class SingletonDemo {

	public static void main(String[] args) {
		
		Singleton x=Singleton.getInstance(); 
		Singleton y=Singleton.getInstance(); 
		Singleton z=Singleton.getInstance(); 
		
		System.out.println("HashCode of x is: "+x.hashCode());
		System.out.println("HashCode of x is: "+y.hashCode());
		System.out.println("HashCode of x is: "+z.hashCode()); 
		
		if(x==y && y==z)
		{
			System.out.println("Three objects are point to the same memory location on the heap i.e; to the same object");
		}
		else
		{
			System.out.println("Three objects do not point to the same memory location on the heap");
		}

	}

}
