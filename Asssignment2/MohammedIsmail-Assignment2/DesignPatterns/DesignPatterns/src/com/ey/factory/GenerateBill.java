package com.ey.factory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class GenerateBill {

	public static void main(String[] args) throws IOException {
		
		GetPlanFactory planFactory=new GetPlanFactory();
		System.out.println("Enter the plan:");
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		String planName=br.readLine();
		
		 System.out.println("Enter the number of units for bill: ");  
	     int units=Integer.parseInt(br.readLine());  
	     
	     Plan p = planFactory.getPlan(planName);   
	     System.out.println("Bill amount is "+planName+" of  "+units+" units is: ");  
         p.getRate();  
         p.calculateBill(units);   
		

	}

}
