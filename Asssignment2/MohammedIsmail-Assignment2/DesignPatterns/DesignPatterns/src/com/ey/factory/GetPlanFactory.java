package com.ey.factory;

public class GetPlanFactory {

	public Plan getPlan(String planType)
	{
		if(planType==null)
		{
			return null;
		}
		else if(planType.equalsIgnoreCase("DOMESTICPLAN"))
		{
			return new DomesticPlan();
		}
		else if(planType.equalsIgnoreCase("COMMERCIALPLAN"))
		{
			return new CommercialPlan();
		}
		else if(planType.equalsIgnoreCase("INSTITUTIONPLAN"))
		{
			return new InstitutionPlan();
		}
	return null;  
	}
}
