package com.ey.demo;

import org.springframework.stereotype.Component;

@Component()
public class College {
	
	public void show() {
		System.out.println("College life is the best memory in each of us.");
	}

}
