package com.ey.demo;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Test {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		/*
		 * ApplicationContext context = new ClassPathXmlApplicationContext("beans.xml");
		 * College college = context.getBean("college",College.class); college.show();
		 */
		ApplicationContext context = new AnnotationConfigApplicationContext(SchoolConfig.class);
		School school = context.getBean("school",School.class);
		School school2 = context.getBean("school",School.class);
		System.out.println(school.hashCode());
		System.out.println(school2.hashCode());
		school.show();
	}

}
