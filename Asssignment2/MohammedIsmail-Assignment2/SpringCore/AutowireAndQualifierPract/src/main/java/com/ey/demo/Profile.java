package com.ey.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

public class Profile {
	@Autowired
	@Qualifier("student1")
	private Student student;
	
	public void show() {
		System.out.println("Name: "+student.getName()+"-->"+"Age: "+student.getAge());
	}

}
