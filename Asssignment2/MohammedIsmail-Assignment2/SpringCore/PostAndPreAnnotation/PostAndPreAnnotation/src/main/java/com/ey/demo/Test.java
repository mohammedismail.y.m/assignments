package com.ey.demo;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Test {

	public static void main(String[] args) {
		ApplicationContext applicationContext = new AnnotationConfigApplicationContext(EmployeeConfiguration.class);
		Employee employee = (Employee) applicationContext.getBean("employee");
		Employee employee1 = (Employee) applicationContext.getBean("employee");
		System.out.println(employee.hashCode());
		System.out.println(employee1.hashCode());
		employee.show();
		

	}

}
