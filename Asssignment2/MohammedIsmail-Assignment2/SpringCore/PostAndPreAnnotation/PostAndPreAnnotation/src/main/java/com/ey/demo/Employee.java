package com.ey.demo;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.springframework.stereotype.Component;

//@Component("emp")
public class Employee {
	
	@PostConstruct
	public void first() {
		System.out.println("Hello All, This is the init method called automatically");
	}
	public void show() {
		System.out.println("This is called only when bean object calls");
	}
	
	@PreDestroy
	private void shutdown() {
		System.out.println("This is called automatically when bean destroyed");
	}
}
