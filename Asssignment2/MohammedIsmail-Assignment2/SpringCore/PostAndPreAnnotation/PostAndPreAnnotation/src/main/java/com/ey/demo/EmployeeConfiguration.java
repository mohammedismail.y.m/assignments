package com.ey.demo;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

@Configuration
//@ComponentScan(basePackages = "com.ey.demo")
public class EmployeeConfiguration {

	@Bean
	@Scope("prototype")
	public Employee employee() {
		return new Employee();
	}
}
