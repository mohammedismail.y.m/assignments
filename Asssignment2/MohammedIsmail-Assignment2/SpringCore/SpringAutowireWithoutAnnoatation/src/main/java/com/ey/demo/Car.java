package com.ey.demo;

import org.springframework.beans.factory.annotation.Autowired;

public class Car {
	
	private Engine engine;
	public void working() {
		if(engine!=null) {
			engine.work();
			System.out.println("Car is fine");
		}else {
			System.out.println("Nope");
		}
		
		
	}
	
	
	public void setEngine(Engine engine) {
		this.engine = engine;
	}
	public Engine getEngine() {
		return engine;
	}
	

}
