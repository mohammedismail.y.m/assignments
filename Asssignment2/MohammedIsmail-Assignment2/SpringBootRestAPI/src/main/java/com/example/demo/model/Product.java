package com.example.demo.model;

public class Product {
	private long productID;
	private String name;
	private long price;
	public long getProductID() {
		return productID;
	}
	public Product(long productID, String name, long price) {
		super();
		this.productID = productID;
		this.name = name;
		this.price = price;
	}
	public void setProductID(long productID) {
		this.productID = productID;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public long getPrice() {
		return price;
	}
	public void setPrice(long price) {
		this.price = price;
	}
}
