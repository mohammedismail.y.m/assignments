package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.Product;
import com.example.demo.service.ProductService;

@RestController  //RestController- for REST API's, when data needed in form of JSON
public class ProductCtrl {
	
	@Autowired
	ProductService productService;
	
	@GetMapping("/products")
	public List<Product> getAllProducts(){
		return productService.getAllProducts();
	}
	
	@GetMapping("/products/{id}")
	public Product getProductById(@PathVariable Long id) {//PathVariable-->converts the format
		return productService.getProductById(id);
	}
	
	@PutMapping("/updateProduct")
	public String updateProductById(@RequestBody Product product) {//RequestBody to give object
		return productService.updateProductById(product);
		
	}
	
	@PostMapping("/saveProduct")
	public String saveProduct(@RequestBody Product product) {
		return productService.saveProduct(product);
		
	}
	
	@DeleteMapping("/deleteProduct/{id}")
	public String deleteProduct(@PathVariable Long id) {
		return productService.deleteProduct(id);
	}
	
	
}
