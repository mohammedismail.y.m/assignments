package com.example.demo.service;

import java.util.List;

import com.example.demo.model.Product;

public interface ProductService {
	
	public List<Product> getAllProducts();
	public Product getProductById(Long id);
	public String updateProductById(Product product);
	public String saveProduct(Product product);
	public String deleteProduct(Long id);
	
	
}
