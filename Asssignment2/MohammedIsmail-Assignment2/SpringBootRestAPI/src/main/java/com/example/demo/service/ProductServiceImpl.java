package com.example.demo.service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.example.demo.model.Product;


@Service
public class ProductServiceImpl implements ProductService {
	
	List<Product> list;
	
	public ProductServiceImpl() {
		list = new ArrayList<Product>();
		list.add(new Product(1L,"Ashirwad",2300L));
		list.add(new Product(2L,"Rice",2400L));
		list.add(new Product(3L,"Pen",2500L));
		
	}
	
	public List<Product> getAllProducts(){
		
		return list;
		
	}
	
	

	@Override
	public Product getProductById(Long id) {
		// TODO Auto-generated method stub
		Product product = null;
		for(Product pro:list) {
			if(pro.getProductID()==id) {
				product=pro;
				break;
			}
		}
		
		return product;
	}

	@Override
	public String updateProductById(Product product) {
		
		list.forEach(p->{
			if(p.getProductID()==product.getProductID()) {
				p.setName(product.getName());
				p.setPrice(product.getPrice());
			}
			
		});
		
		
		return "Update!!!";
	}

	@Override
	public String saveProduct(Product product) {
		list.add(product);
		// TODO Auto-generated method stub
		return "Inserted";
	}

	@Override
	public String deleteProduct(Long id) {
		list=list.stream().filter(p->p.getProductID()!=id).collect(Collectors.toList());
		// TODO Auto-generated method stub
		return "Deleted";
	}
}
